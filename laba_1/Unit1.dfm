object Form1: TForm1
  Left = 270
  Top = 123
  Width = 840
  Height = 572
  Caption = 'Form1'
  Color = clHotLight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 88
    Top = 24
    Width = 137
    Height = 25
    Alignment = taCenter
    AutoSize = False
    Caption = 'temp'
    Color = clWindow
    ParentColor = False
  end
  object Label2: TLabel
    Left = 88
    Top = 64
    Width = 137
    Height = 25
    Alignment = taCenter
    AutoSize = False
    Caption = 'roomtemp'
    Color = clYellow
    ParentColor = False
  end
  object Label3: TLabel
    Left = 88
    Top = 112
    Width = 137
    Height = 25
    Alignment = taCenter
    AutoSize = False
    Caption = 'tmax'
    Color = clYellow
    ParentColor = False
  end
  object Label4: TLabel
    Left = 88
    Top = 152
    Width = 137
    Height = 25
    Alignment = taCenter
    AutoSize = False
    Caption = 'n'
    Color = clYellow
    ParentColor = False
  end
  object Label5: TLabel
    Left = 88
    Top = 192
    Width = 137
    Height = 25
    Alignment = taCenter
    AutoSize = False
    Caption = 'r '
    Color = clYellow
    ParentColor = False
  end
  object Label6: TLabel
    Left = 40
    Top = 400
    Width = 134
    Height = 13
    Caption = #1059#1087#1072#1076#1105#1090' '#1085#1072' 20 '#1075#1088#1072#1076#1091#1089#1086#1074' '#1079#1072':'
    Color = clYellow
    ParentColor = False
  end
  object Label7: TLabel
    Left = 40
    Top = 432
    Width = 116
    Height = 13
    Caption = #1059#1087#1072#1076#1105#1090' '#1076#1086' 40 '#1075#1088#1072#1076#1091#1089#1086#1074
    Color = clYellow
    ParentColor = False
  end
  object Chart1: TChart
    Left = 400
    Top = 16
    Width = 400
    Height = 250
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      'TChart')
    BottomAxis.Automatic = False
    BottomAxis.AutomaticMaximum = False
    BottomAxis.AutomaticMinimum = False
    BottomAxis.Maximum = 100.000000000000000000
    LeftAxis.Automatic = False
    LeftAxis.AutomaticMaximum = False
    LeftAxis.AutomaticMinimum = False
    LeftAxis.Maximum = 100.000000000000000000
    View3D = False
    TabOrder = 0
    object Series1: TFastLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      LinePen.Color = clRed
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
  end
  object Edit1: TEdit
    Left = 240
    Top = 24
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '100'
  end
  object Edit2: TEdit
    Left = 240
    Top = 64
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '20'
  end
  object Button1: TButton
    Left = 104
    Top = 272
    Width = 75
    Height = 25
    Caption = #1055#1086#1089#1090#1088#1086#1080#1090#1100
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 256
    Top = 272
    Width = 75
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    TabOrder = 4
    OnClick = Button2Click
  end
  object Edit3: TEdit
    Left = 240
    Top = 112
    Width = 121
    Height = 21
    TabOrder = 5
    Text = '100'
  end
  object Edit4: TEdit
    Left = 240
    Top = 152
    Width = 121
    Height = 21
    TabOrder = 6
    Text = '100'
  end
  object Edit5: TEdit
    Left = 240
    Top = 200
    Width = 121
    Height = 21
    TabOrder = 7
    Text = '0,1'
  end
  object Edit6: TEdit
    Left = 192
    Top = 400
    Width = 121
    Height = 21
    TabOrder = 8
  end
  object Edit7: TEdit
    Left = 192
    Top = 432
    Width = 121
    Height = 21
    TabOrder = 9
  end
  object rg1: TRadioGroup
    Left = 440
    Top = 368
    Width = 185
    Height = 105
    Caption = 'rg1'
    TabOrder = 10
  end
end
