unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TeEngine, Series, ExtCtrls, TeeProcs, Chart;

type
  TForm1 = class(TForm)
    Chart1: TChart;
    Series1: TFastLineSeries;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    Button2: TButton;
    Edit3: TEdit;
    Edit4: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Label6: TLabel;
    Label7: TLabel;
    rg1: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
 var
  t,tmax,dtemp,temp,roomtemp,r,dt: real;
  i,n: integer;
 begin
//  t:=0;
//  temp:=83;
//  roomtemp:=20;
//  r:=0.1;
//  n:=100;
//  tmax:=2;
//  dt:=tmax/n;
//  for i:=1 to n do
//   begin
//    dtemp:=-r*(temp-roomtemp);
//    temp:=temp+dtemp*dt;
//    t:= t+dt;
//    Chart1.Series[0].AddXY (t,temp);
//   end;
 end;
procedure TForm1.Button2Click(Sender: TObject);
begin
Chart1.Series[0].Clear;
end;

procedure TForm1.Button1Click(Sender: TObject);
 var
  t,tmax,dtemp,temp,roomtemp,r,dt: real;
  i,n: integer;
 begin
  t:=0;
  temp:=StrToFloat(Edit1.Text);
  roomtemp:=StrToFloat(Edit2.Text);
  tmax:=StrToFloat(Edit3.Text);
  n:=StrToInt(Edit4.Text);
  r:=StrToFloat(Edit5.Text);
  dt:=tmax/n;
  dtemp:=0;
  for i:=1 to n do
   begin
    dtemp:=-r*(temp-roomtemp);
    temp:=temp+dtemp*dt;
    t:= t+dt;
    Chart1.Series[0].AddXY (t,temp);
    if temp<=90 then Edit6.Text:=FloatToStr(t);
    if 100-temp<=20 then Edit7.Text:=FloatToStr(t);
   end;
end;


end.
