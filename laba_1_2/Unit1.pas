unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, TeEngine, Series, TeeProcs, Chart, Buttons;

type
  TForm1 = class(TForm)
    lbl1: TLabel;
    pnl1: TPanel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    edt1: TEdit;
    edt2: TEdit;
    edt3: TEdit;
    lbl6: TLabel;
    edt6: TEdit;
    cht1: TChart;
    lnsrsSeries1: TLineSeries;
    btn1: TBitBtn;
    btn2: TBitBtn;
    btn3: TBitBtn;
    lbl7: TLabel;
    lbl8: TLabel;
    edt4: TEdit;
    pnl2: TPanel;
    lbl9: TLabel;
    edt5: TEdit;
    lbl10: TLabel;
    lbl11: TLabel;
    edt7: TEdit;
    lbl12: TLabel;
    edt8: TEdit;
    lbl13: TLabel;
    edt9: TEdit;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1Click(Sender: TObject);
var
   begin_time, maximum_time, dtemp, temp, roomtemp, r, dt, last_temperature, to_temperature,
    step_temperature: real;
  i, n: integer;
begin
  begin_time := 0;
  temp := StrToFloat(edt1.Text);
  roomtemp := StrToFloat(edt2.Text);
  maximum_time := StrToFloat(edt3.Text);
  n := StrToInt(edt4.Text);
  r := StrToFloat(edt6.Text);
  to_temperature := StrToFloat(edt8.Text);
  step_temperature := StrToFloat(edt5.Text);
  last_temperature := temp;
  dt := maximum_time / n;
  dtemp := 0;
  for i := 1 to n do
  begin
    dtemp := -r * (temp - roomtemp);
    temp := temp + dtemp * dt;
    begin_time := begin_time + dt;
    cht1.Series[0].AddXY(begin_time, temp);
    if (temp >=to_temperature) and (temp <= last_temperature) then
    begin
      edt9.Text := FloatToStr(begin_time);
    end;
    if 100 - temp <= step_temperature then
    begin
      edt7.Text := FloatToStr(begin_time);
    end;
    last_temperature := temp;
  end;

end;

procedure TForm1.btn2Click(Sender: TObject);
begin
  cht1.Series[0].Clear;
end;

procedure TForm1.btn3Click(Sender: TObject);
begin
  Form1.Close;
end;
end.

