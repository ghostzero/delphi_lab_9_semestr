object Form1: TForm1
  Left = 149
  Top = 47
  Width = 843
  Height = 357
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 40
    Top = 80
    Width = 7
    Height = 13
    Caption = 'X'
  end
  object Label2: TLabel
    Left = 176
    Top = 80
    Width = 7
    Height = 13
    Caption = 'Y'
  end
  object lbl1: TLabel
    Left = 0
    Top = 0
    Width = 745
    Height = 73
    Caption = 
      #1051#1072#1073#1086#1088#1072#1090#1086#1088#1085#1072#1103' '#1088#1072#1073#1086#1090#1072' '#8470'4 '#1055#1088#1080#1073#1083#1080#1078#1077#1085#1085#1099#1077' '#1084#1077#1090#1086#1076#1099' '#1074#1099#1095#1080#1089#1083#1077#1085#1080#1103'. '#13#10#1048#1085#1090#1077#1088#1087#1086 +
      #1083#1103#1094#1080#1103' '#1084#1077#1090#1086#1084' '#1051#1072#1075#1088#1072#1085#1078#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object lbl2: TLabel
    Left = 677
    Top = 103
    Width = 65
    Height = 26
    Caption = #1055#1088#1080#1088#1072#1097#1077#1085#1080#1077#13#10' '#1074#1088#1077#1084#1077#1085#1080':'
  end
  object Edit1: TEdit
    Left = 0
    Top = 96
    Width = 121
    Height = 21
    TabOrder = 0
    Text = '1'
  end
  object Edit2: TEdit
    Left = 0
    Top = 120
    Width = 121
    Height = 21
    TabOrder = 1
    Text = '2'
  end
  object Edit3: TEdit
    Left = 0
    Top = 144
    Width = 121
    Height = 21
    TabOrder = 2
    Text = '3'
  end
  object Edit4: TEdit
    Left = 0
    Top = 168
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '4'
  end
  object Edit5: TEdit
    Left = 0
    Top = 192
    Width = 121
    Height = 21
    TabOrder = 4
    Text = '5'
  end
  object Edit6: TEdit
    Left = 128
    Top = 96
    Width = 121
    Height = 21
    TabOrder = 5
    Text = '1'
  end
  object Edit7: TEdit
    Left = 128
    Top = 120
    Width = 121
    Height = 21
    TabOrder = 6
    Text = '4'
  end
  object Edit8: TEdit
    Left = 128
    Top = 144
    Width = 121
    Height = 21
    TabOrder = 7
    Text = '9'
  end
  object Edit9: TEdit
    Left = 128
    Top = 168
    Width = 121
    Height = 21
    TabOrder = 8
    Text = '16'
  end
  object Edit10: TEdit
    Left = 128
    Top = 192
    Width = 121
    Height = 21
    TabOrder = 9
    Text = '25'
  end
  object Chart1: TChart
    Left = 280
    Top = 88
    Width = 392
    Height = 201
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      'TChart')
    View3D = False
    TabOrder = 10
    object Series1: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
  end
  object Button1: TButton
    Left = 8
    Top = 224
    Width = 105
    Height = 25
    Caption = #1055#1086#1089#1090#1088#1086#1080#1090#1100
    TabOrder = 11
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 143
    Top = 226
    Width = 97
    Height = 24
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1075#1088#1072#1092#1080#1082
    TabOrder = 12
    OnClick = Button2Click
  end
  object edt1: TEdit
    Left = 744
    Top = 104
    Width = 65
    Height = 21
    TabOrder = 13
    Text = '0,001'
  end
end
