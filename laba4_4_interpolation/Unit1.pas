unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Math, TeEngine, Series, StdCtrls, ExtCtrls, TeeProcs, Chart,
  Grids;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Edit7: TEdit;
    Edit8: TEdit;
    Edit9: TEdit;
    Edit10: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Chart1: TChart;
    Button1: TButton;
    Button2: TButton;
    Series1: TLineSeries;
    edt1: TEdit;
    lbl1: TLabel;
    lbl2: TLabel;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var x, dx, fx,a,b,c,d,e,f,g,i,j,k: double;
begin
dx:= StrToFloat (edt1.Text) ;
x:=StrToFloat (Edit1.Text) ;
a:=StrToFloat (Edit1.Text);
b:=StrToFloat (Edit2.Text);
c:=StrToFloat (Edit3.Text);
d:=StrToFloat (Edit4.Text);
e:=StrToFloat (Edit5.Text);
f:=StrToFloat (Edit6.Text);
g:=StrToFloat (Edit7.Text);
i:=StrToFloat (Edit8.Text);
j:=StrToFloat (Edit9.Text);
k:=StrToFloat (Edit10.Text);
While (x <= StrToFloat (Edit5.Text)) Do
begin
fx:= f*((x-b)*(x-c)*(x-d)*(x-e))/((a-b)*(a-c)*(a-d)*(a-e))+
       g*((x-a)*(x-c)*(x-d)*(x-e))/((b-a)*(b-c)*(b-d)*(b-e))+
       i*((x-a)*(x-b)*(x-d)*(x-e))/((c-a)*(c-b)*(c-d)*(c-e))+
       j*((x-a)*(x-b)*(x-c)*(x-e))/((d-a)*(d-b)*(d-c)*(d-e))+
       k*((x-a)*(x-b)*(x-c)*(x-d))/((e-a)*(e-b)*(e-c)*(e-d));
x:=x+dx;
Chart1.SeriesList[0].AddXY (x, fx);
end;
end;



procedure TForm1.Button2Click(Sender: TObject);
begin
Chart1.SeriesList[0].Clear;
end;



end.
