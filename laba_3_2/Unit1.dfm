object Form1: TForm1
  Left = 191
  Top = 108
  Width = 878
  Height = 471
  Caption = #1051#1072#1073#1086#1088#1072#1090#1086#1088#1085#1072#1103' '#1088#1072#1073#1086#1090#1072' '#8470'3 '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel
    Left = 22
    Top = 336
    Width = 91
    Height = 13
    Caption = #1069#1081#1083#1077#1088#1072' - '#1050#1088#1086#1084#1077#1083#1103
  end
  object Label7: TLabel
    Left = 157
    Top = 336
    Width = 40
    Height = 13
    Caption = ' '#1069#1081#1083#1077#1088#1072
  end
  object Label8: TLabel
    Left = 264
    Top = 338
    Width = 31
    Height = 13
    Caption = #1042#1077#1088#1083#1077
  end
  object lbl1: TLabel
    Left = 0
    Top = 0
    Width = 842
    Height = 48
    Caption = 
      #1051#1072#1073#1086#1088#1072#1090#1086#1088#1085#1072#1103' '#1088#1072#1073#1086#1090#1072' '#8470'3. '#1042#1086#1083#1085#1086#1074#1099#1077' '#1103#1074#1083#1077#1085#1080#1103' '#1080' '#1080#1093' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103' '#1089' '#1087#1086#1084#1086 +
      #1097#1100#1102' '#1084#1077#1090#1086#1076#1086#1074#13#10' '#1069#1081#1083#1077#1088#1072'-'#1050#1088#1086#1084#1077#1088#1072', '#1069#1081#1083#1077#1088#1072', '#1042#1077#1088#1083#1077'.'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Button1: TButton
    Left = 24
    Top = 352
    Width = 81
    Height = 25
    Caption = #1055#1086#1089#1090#1088#1086#1080#1090#1100
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 24
    Top = 376
    Width = 81
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    TabOrder = 1
    OnClick = Button2Click
  end
  object Chart1: TChart
    Left = 0
    Top = 63
    Width = 625
    Height = 250
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      'TChart')
    View3D = False
    TabOrder = 2
    object Series1: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
    object Series2: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clGreen
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
    object Series3: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clYellow
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
    object Series4: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clBlue
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
  end
  object Button3: TButton
    Left = 132
    Top = 352
    Width = 89
    Height = 25
    Caption = #1055#1086#1089#1090#1088#1086#1080#1090#1100
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 132
    Top = 376
    Width = 89
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    TabOrder = 4
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 239
    Top = 354
    Width = 81
    Height = 25
    Caption = #1055#1086#1089#1090#1088#1086#1080#1090#1100
    TabOrder = 5
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 239
    Top = 378
    Width = 81
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    TabOrder = 6
    OnClick = Button6Click
  end
  object pnl1: TPanel
    Left = 624
    Top = 64
    Width = 233
    Height = 249
    TabOrder = 7
    object lbl2: TLabel
      Left = 65
      Top = 48
      Width = 96
      Height = 13
      Caption = #1059#1075#1083#1086#1074#1072#1103' '#1089#1082#1086#1088#1086#1089#1090#1100':'
    end
    object lbl3: TLabel
      Left = 58
      Top = 72
      Width = 103
      Height = 13
      Caption = #1051#1080#1085#1077#1081#1085#1072#1103' '#1089#1082#1086#1088#1086#1089#1090#1100':'
    end
    object lbl4: TLabel
      Left = 41
      Top = 96
      Width = 120
      Height = 13
      Caption = #1053#1072#1095#1072#1083#1100#1085#1072#1103' '#1082#1086#1086#1088#1076#1080#1085#1072#1090#1072':'
    end
    object lbl5: TLabel
      Left = 23
      Top = 118
      Width = 138
      Height = 26
      Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1074#1088#1077#1084#1077#1085#1085#1086#1081#13#10' '#1087#1088#1086#1084#1077#1078#1091#1090#1086#1082':'
    end
    object lbl6: TLabel
      Left = 46
      Top = 150
      Width = 115
      Height = 13
      Caption = #1055#1088#1080#1088#1072#1097#1077#1085#1080#1077' '#1074#1088#1077#1084#1077#1085#1080':'
    end
    object edt1: TEdit
      Left = 171
      Top = 44
      Width = 49
      Height = 21
      TabOrder = 0
      Text = '2'
    end
    object edt2: TEdit
      Left = 171
      Top = 70
      Width = 49
      Height = 21
      TabOrder = 1
      Text = '1'
    end
    object edt3: TEdit
      Left = 171
      Top = 97
      Width = 49
      Height = 21
      TabOrder = 2
      Text = '0'
    end
    object edt4: TEdit
      Left = 171
      Top = 121
      Width = 49
      Height = 21
      TabOrder = 3
      Text = '10'
    end
    object edt5: TEdit
      Left = 171
      Top = 148
      Width = 49
      Height = 21
      TabOrder = 4
      Text = '0,01'
    end
  end
end
