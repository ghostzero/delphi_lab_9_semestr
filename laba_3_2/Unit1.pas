unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, TeEngine, Series, ExtCtrls, TeeProcs, Chart;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Chart1: TChart;
    Series1: TLineSeries;
    Label6: TLabel;
    Label7: TLabel;
    Button3: TButton;
    Button4: TButton;
    Series2: TLineSeries;
    Label8: TLabel;
    Button5: TButton;
    Button6: TButton;
    Series3: TLineSeries;
    Series4: TLineSeries;
    lbl1: TLabel;
    pnl1: TPanel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    lbl6: TLabel;
    edt1: TEdit;
    edt2: TEdit;
    edt3: TEdit;
    edt4: TEdit;
    edt5: TEdit;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);


  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  w, v, x, t, dt, tmax: double;
begin

  t := 0;
  w := StrToFloat(edt1.Text);
  v := StrToFloat(edt2.Text);
  x := StrToFloat(edt3.Text);
  tmax := StrToFloat(edt4.Text);
  dt := StrToFloat(edt5.Text);

  while (t <= tmax) do
  begin
    t := t + dt;
    v := v - w * w * x * dt;
    x := x + v * dt;
    Chart1.SeriesList[0].AddXY(t, x);

  end;

end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Chart1.SeriesList[0].Clear;
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  w, v, x, y, t, dt, tmax: double;
begin
  t := 0;
  w := StrToFloat(edt1.Text);
  v := StrToFloat(edt2.Text);
  x := StrToFloat(edt3.Text);
  tmax := StrToFloat(edt4.Text);
  dt := StrToFloat(edt5.Text);
  y := x;

  while (t <= tmax) do

  begin
    t := t + dt;
    y:=x;
    x := x + v * dt;
    v := v - w * w * y * dt;
//    y := x - v * dt;


    Chart1.SeriesList[1].AddXY(t, x);

  end;

end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  Chart1.SeriesList[1].Clear;
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  w, v1, v2, x1, x2, x3, t, dt, tmax: double;
begin
  t := 0;
  w := StrToFloat(edt1.Text);
  v1 := StrToFloat(edt2.Text);
  x1 := StrToFloat(edt3.Text);
  tmax := StrToFloat(edt4.Text);
  dt := StrToFloat(edt5.Text);
  v2:=0;
  while (t <= tmax) do
  begin
    t := t + dt;
    x1 := x1 + v1 * dt;
    v1 := v1 - w * w * x1 * dt;
    x2 := x1 + v2 * dt;
    v2 := v2 - w * w * x2 * dt;
    x3 := x1 + 2 * v2 * dt;
    Chart1.SeriesList[2].AddXY(t, x3);
  end;

end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  Chart1.SeriesList[2].Clear;

end;
end.

