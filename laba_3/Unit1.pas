unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, TeEngine, Series, TeeProcs, Chart, Buttons;

type
  TForm1 = class(TForm)
    lbl1: TLabel;
    pnl1: TPanel;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    edt1: TEdit;
    edt2: TEdit;
    edt3: TEdit;
    lbl6: TLabel;
    edt6: TEdit;
    cht1: TChart;
    lnsrsSeries1: TLineSeries;
    btn1: TBitBtn;
    btn2: TBitBtn;
    btn3: TBitBtn;
    lbl7: TLabel;
    lbl8: TLabel;
    edt4: TEdit;
    lnsrsSeries2: TLineSeries;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1Click(Sender: TObject);
var
  g, y, y_1, x, x_1, v, v_1, vx, vx_1, vy, vy_1, angle, alfa, t, dt, vst:
    double;
begin
  g := 9.8;
  t := 0;
  x := 0;
  x_1 := x;
  y := StrToFloat(edt1.Text);
  y_1 := y;
  v := StrToFloat(edt2.Text);
  v_1 := v;
  angle := StrToFloat(edt3.Text);
  dt := StrToFloat(edt4.Text);
  alfa := StrToFloat(edt6.Text);
  vst := v_1 * v_1;
  while (y >= 0) do

  begin
    vx := v * cos((angle / 180) * 3.14);
    vy := v * sin((angle / 180) * 3.14) - g * t;
    x := x + vx * dt;
    y := y + vy * dt;
    vx_1 := v_1 * cos((angle / 180) * 3.14) - alfa * (vst) * cos((angle / 180) *
      3.14) * dt;
    vy_1 := v_1 * sin((angle / 180) * 3.14) - g * t - alfa * (vst) * sin((angle
      /
      180) * 3.14) * dt;
    vst := vx_1 * vx_1 + vy_1 * vy_1;
    t := t + dt;
    x_1 := x_1 + vx_1 * dt;
    y_1 := y_1 + vy_1 * dt;
    cht1.Series[0].AddXY(x, y);
    cht1.Series[1].AddXY(x_1, y_1);
  end;

end;

procedure TForm1.btn2Click(Sender: TObject);
begin
  cht1.Series[0].Clear;
  cht1.Series[1].Clear;
end;

procedure TForm1.btn3Click(Sender: TObject);
begin
  Form1.Close;
end;
end.

