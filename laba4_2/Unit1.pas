unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TeEngine, Series, ExtCtrls, TeeProcs, Chart, StdCtrls, Grids, Math;

type
  TForm1 = class(TForm)
    SG1: TStringGrid;
    Ed1: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Ed2: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Button1: TButton;
    Button2: TButton;
    Chart1: TChart;
    Series1: TLineSeries;
    Button3: TButton;
    Edit1: TEdit;
    Label5: TLabel;
    Edit2: TEdit;
    Label6: TLabel;
    Edit3: TEdit;
    Label7: TLabel;
    Button4: TButton;
    Series2: TLineSeries;
    Label8: TLabel;
    Label9: TLabel;
    Button5: TButton;
    Label10: TLabel;
    Button7: TButton;
    Series3: TLineSeries;
    Series4: TLineSeries;
    //    TArray: Array of Double;
    B1: TButton;
    lbl2: TLabel;
    lbl3: TLabel;
    lbl4: TLabel;
    lbl5: TLabel;
    edt2: TEdit;
    edt3: TEdit;
    edt4: TEdit;
    StringGrid4: TStringGrid;
    StringGrid2: TStringGrid;
    StringGrid3: TStringGrid;
    btn1: TButton;
    btn2: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure B1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    //    procedure get_polynom_3(Sender: TObject);
    //    procedure get_polynom(Sender: TObject);
        //    procedure booblesort(var A: TArray; min, max: Integer);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  i: integer;
  SumX, SumX2, SumY, SumXY, n, A1: double;
  //  sort_array_argument, sort_array_function: array of double;
begin

  n := Sg1.RowCount;
  SumY := 0;
  SumX := 0;
  SumX2 := 0;
  SumXY := 0;
  //  for i := 0 to SG1.RowCount - 1 do
  //  begin
  //    sort_array_argument[i] := StrToFloat(SG1.Cells[0, i]);
  //    sort_array_function[i] := StrToFloat(SG1.Cells[1, i]);
  //  end;

  for i := 0 to SG1.RowCount - 1 do
  begin
    SumX := SumX + StrToFloat(SG1.Cells[0, i]);
  end;

  for i := 0 to SG1.RowCount - 1 do
  begin
    SumX2 := SumX2 + Power(2, StrToFloat(SG1.Cells[0, i]))
  end;

  for i := 0 to SG1.RowCount - 1 do
  begin
    SumY := SumY + StrToFloat(SG1.Cells[1, i]);
  end;

  Ed1.Text := FloatToStr(SumY / Sg1.RowCount);

  for i := 0 to Sg1.RowCount - 1 do
  begin
    SumXY := SumXY + StrToFloat(SG1.Cells[0, i]) * StrToFloat(SG1.Cells[1, i]);
  end;

  Edit2.Text := FloatToStr((SumY * SumX2 - SumX * SumXY) / (n * SumX2 - SumX *
    SumX));
  A1 := (n * SumXY - SumY * SumX) / (n * SumX2 - SumX * SumX);
  A1 := StrToFloat(Ed2.Text);
  Edit3.Text := Edit2.Text + '+' + ' ' + Ed2.Text + '*x';
  Ed2.Text := FloatToStr(A1);
end;

//

procedure TForm1.Button3Click(Sender: TObject);
var
  i: integer;
  x, dx, xmin, xmax, p, A0, A1, f: Double;
begin
  dx := StrToFloat(Edit1.Text);
  x := 0;
  xmax := StrToFloat(Sg1.Cells[0, 4]);
  A0 := StrToFloat(Edit2.Text);
  A1 := StrToFloat(Ed2.Text);
  //  A2 := StrToFloat(Ed2.Text);
  f := StrToFloat(Ed1.Text);
  xmax := 1;
  xmin := 1;
  for i := 0 to SG1.RowCount - 1 do
  begin
    if xmax < StrToFloat(SG1.Cells[0, i]) then
      xmax := StrToFloat(SG1.Cells[0, i]);
    if xmin > StrToFloat(SG1.Cells[0, i]) then
      xmin := StrToFloat(SG1.Cells[0, i]);
  end;

  while (x <= xmax) do
  begin
    x := x + dx;
    p := A0 + A1 * x;
    Chart1.SeriesList[0].AddXY(x, p);
    Chart1.SeriesList[1].AddXY(x, f);
  end;

end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  Edit3.Text := '                                               ';
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Chart1.SeriesList[0].Clear;
  Chart1.SeriesList[1].Clear;
  Chart1.SeriesList[2].Clear;
  Chart1.SeriesList[3].Clear;
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  x, dx, fx: double;
begin
  x := 0;
  dx := StrToFloat(Edit1.Text);
  while (x <= StrToFloat(Sg1.Cells[0, 4])) do
  begin
    x := x + dx;
    fx := x * x;
    Chart1.SeriesList[2].AddXY(x, fx);
  end;
end;

procedure TForm1.Button7Click(Sender: TObject);
var
  x, dx, fx: double;
begin
  x := 0;
  dx := StrToFloat(Edit1.Text);
  while (x <= StrToFloat(Sg1.Cells[0, 4])) do
  begin
    x := x + dx;
    fx := x * x * x;
    Chart1.SeriesList[3].AddXY(x, fx);
  end;

end;

procedure TForm1.B1Click(Sender: TObject);
begin
  Sg1.RowCount := Sg1.RowCount + 1;
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  i: integer;
begin
  //SG1.Cells[1, i]
  for i := 0 to Sg1.RowCount - 1 do
  begin
    SG1.Cells[0, i] := IntToStr(i + 1);
    SG1.Cells[1, i] := IntToStr((i + 1) * (i + 1));
  end;
end;

//procedure get_polynom(Sender: TObject);
//
//var
//  i, count_rows: integer;
//  B_00, B_01_10, B_02_20, B_11, B_21_12, B_22: double;
//begin
//  B_00 := 0;
//  B_01_10 := 0;
//  B_02_20 := 0;
//  B_11 := 0;
//  B_21_12 := 0;
//  B_22 := 0;
//  //  count_rows:=Sg1.RowCount;
//  count_rows := 2;
//  for i := 0 to count_rows - 1 do
//  begin
//    B_00 := B_00 + StrToFloat(SG1.Cells[0, i]);
//  end;
//
//  for i := 0 to count_rows - 1 do
//  begin
//    B_01_10 := B_01_10 + Power(2, StrToFloat(SG1.Cells[0, i]))
//  end;
//
//  for i := 0 to count_rows - 1 do
//  begin
//    B_02_20 := B_02_20 + StrToFloat(SG1.Cells[0, i]);
//  end;
//
//  for i := 0 to count_rows - 1 do
//  begin
//    B_11 := B_11 + StrToFloat(SG1.Cells[0, i]);
//  end;
//
//  for i := 0 to count_rows - 1 do
//  begin
//    B_21_12 := B_21_12 + Power(2, StrToFloat(SG1.Cells[0, i]))
//  end;
//
//  for i := 0 to count_rows - 1 do
//  begin
//    B_22 := B_22 + StrToFloat(SG1.Cells[0, i]);
//  end;
//
//  Ed1.Text := FloatToStr(SumY / Sg1.RowCount);
//
//  for i := 0 to Sg1.RowCount - 1 do
//  begin
//    SumXY := SumXY + StrToFloat(SG1.Cells[0, i]) * StrToFloat(SG1.Cells[1, i]);
//  end;
//
//end;

procedure TForm1.btn1Click(Sender: TObject);
var
  i, count_rows: integer;
  B_00, B_01_10, B_02_20, B_11, B_21_12, B_22, C_0, C_1, C_2: double;
begin
  B_00 := 0;
  B_01_10 := 0;
  B_02_20 := 0;
  B_11 := 0;
  B_21_12 := 0;
  B_22 := 6;
  C_0 := 0;
  C_1 := 0;
  C_2 := 0;
  count_rows := Sg1.RowCount;
  //  count_rows := 2;
  for i := 0 to count_rows - 1 do
  begin
    B_00 := B_00 + IntPower(SG1.Cells[0, i], 4);
  end;

  for i := 0 to count_rows - 1 do
  begin
    B_01_10 := B_01_10 + IntPower(StrToFloat(SG1.Cells[0, i]), 3);
  end;

  for i := 0 to count_rows - 1 do
  begin
    B_02_20 := B_02_20 + IntPower(StrToFloat(SG1.Cells[0, i]), 2);
  end;

  for i := 0 to count_rows - 1 do
  begin
    B_21_12 := B_21_12 + StrToFloat(SG1.Cells[0, i]);
  end;

  for i := 0 to count_rows - 1 do
  begin
    B_11 := B_11 + IntPower(StrToFloat(SG1.Cells[0, i]), 2);
  end;

//  for i := 0 to count_rows - 1 do
//  begin
//    B_22 := B_22 + StrToFloat(SG1.Cells[0, i]);
//  end;

  for i := 0 to count_rows - 1 do
  begin
    C_0 := C_0 + StrToFloat(SG1.Cells[1, i])*IntPower(StrToFloat(SG1.Cells[0, i]),2);
  end;
  for i := 0 to count_rows - 1 do
  begin
    C_1 := C_1 + StrToFloat(SG1.Cells[1, i])*StrToFloat(SG1.Cells[0, i]);
  end;
  for i := 0 to count_rows - 1 do
  begin
    C_2 := C_2 + StrToFloat(SG1.Cells[1, i]);
  end;

  //  Ed1.Text := FloatToStr(SumY / Sg1.RowCount);

  //  for i := 0 to Sg1.RowCount - 1 do
  //  begin
  //    SumXY := SumXY + StrToFloat(SG1.Cells[0, i]) * StrToFloat(SG1.Cells[1, i]);
  //  end;
end;

procedure TForm1.btn2Click(Sender: TObject);
const
  n = 3;
var
  i, j, k: byte;
  //  n: integer;
  a, a1: array[1..n, 1..n] of real;
  b, b1, x, y, m: array[1..n] of real;
  h, g: real;
begin
  //�������� ������� �������������
  a[1, 1] := 2.34;
  a[1, 2] := -4.21;
  a[1, 3] := -11.61;
  a[2, 1] := 8.04;
  a[2, 2] := 5.22;
  a[2, 3] := 0.27;
  a[3, 1] := 3.92;
  a[3, 2] := -7.99;
  a[3, 3] := 8.37;
  a1 := a; //�������� �� ��� ��������, ������� ���������
  //�� �� ��������� �����
  b[1] := 14.41;
  b[2] := -6.44;
  b[3] := 55.56;
  b1 := b;
  for i := 1 to n do
    for j := 1 to n do
      StringGrid4.Cells[j - 1, i - 1] := FloattostrF(a[i, j], ffFixed, 0, 2);
  for i := 0 to n - 1 do
  begin
    StringGrid2.Cells[0, i] := 'X' + inttostr(i + 1) + '=';
    StringGrid4.Cells[n, i] := FloattostrF(b[i + 1], ffFixed, 0, 2);
  end;
  {���������� ������� � ������������ ����}
  for k := 1 to n do
  begin
    for j := k + 1 to n do
    begin
      h := a[j, k] / a[k, k];
      for i := k to n do
        a[j, i] := a[j, i] - h * a[k, i];
      b[j] := b[j] - h * b[k];
    end;
  end;
  {���������� ������}
  for k := n downto 1 do
  begin
    h := 0;
    for j := k + 1 to n do
    begin
      g := a[k, j] * x[j];
      h := h + g;
    end;
    x[k] := (b[k] - h) / a[k, k];
  end;

  for i := 1 to n do
    StringGrid2.Cells[1, i - 1] := FloattostrF(x[i], ffFixed, 0, 2);
  //��������
  for i := 1 to n do
  begin
    y[i] := 0;
    for j := 1 to n do
      y[i] := y[i] + a1[i, j] * x[j];
  end;
  for i := 1 to n do
    m[i] := abs(b1[i] - y[i]); //�������, � ���� ��� �� �����
  for i := 1 to n do
    StringGrid3.Cells[0, i - 1] := FloattostrF(m[i], ffFixed, 0, 6);
end;

end.

