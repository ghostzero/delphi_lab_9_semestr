object Form1: TForm1
  Left = 255
  Top = 190
  Width = 782
  Height = 492
  VertScrollBar.Position = 105
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 576
    Top = -81
    Width = 13
    Height = 13
    Caption = 'P0'
  end
  object Label2: TLabel
    Left = 600
    Top = -9
    Width = 13
    Height = 13
    Caption = 'A1'
  end
  object Label3: TLabel
    Left = 449
    Top = -97
    Width = 9
    Height = 13
    Caption = 'Xi'
  end
  object Label4: TLabel
    Left = 512
    Top = -97
    Width = 8
    Height = 13
    Caption = 'Fi'
  end
  object Label5: TLabel
    Left = 368
    Top = 319
    Width = 11
    Height = 13
    Caption = 'dx'
  end
  object Label6: TLabel
    Left = 600
    Top = -33
    Width = 13
    Height = 13
    Caption = 'A0'
  end
  object Label7: TLabel
    Left = 352
    Top = 287
    Width = 24
    Height = 13
    Caption = 'P1(x)'
  end
  object Label8: TLabel
    Left = 576
    Top = -17
    Width = 13
    Height = 13
    Caption = 'P1'
  end
  object Label9: TLabel
    Left = 424
    Top = 167
    Width = 71
    Height = 13
    Caption = 'Checking (x^2)'
  end
  object Label10: TLabel
    Left = 424
    Top = 223
    Width = 71
    Height = 13
    Caption = 'Checking (x^3)'
  end
  object lbl2: TLabel
    Left = 600
    Top = 114
    Width = 13
    Height = 13
    Caption = 'A1'
  end
  object lbl3: TLabel
    Left = 600
    Top = 90
    Width = 13
    Height = 13
    Caption = 'A0'
  end
  object lbl4: TLabel
    Left = 576
    Top = 106
    Width = 13
    Height = 13
    Caption = 'P2'
  end
  object lbl5: TLabel
    Left = 600
    Top = 140
    Width = 13
    Height = 13
    Caption = 'A2'
  end
  object SG1: TStringGrid
    Left = 440
    Top = -73
    Width = 85
    Height = 185
    ColCount = 2
    DefaultColWidth = 40
    FixedCols = 0
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 0
  end
  object Ed1: TEdit
    Left = 656
    Top = -81
    Width = 81
    Height = 21
    TabOrder = 1
  end
  object Ed2: TEdit
    Left = 656
    Top = -9
    Width = 81
    Height = 21
    TabOrder = 2
  end
  object Button1: TButton
    Left = 568
    Top = 21
    Width = 81
    Height = 25
    Caption = 'Calculate'
    TabOrder = 3
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 656
    Top = 53
    Width = 81
    Height = 25
    Caption = 'Clear'
    TabOrder = 4
    OnClick = Button2Click
  end
  object Chart1: TChart
    Left = 8
    Top = -97
    Width = 377
    Height = 281
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    Title.Text.Strings = (
      'TChart')
    View3D = False
    TabOrder = 5
    object Series1: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clRed
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
    object Series2: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clGreen
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
    object Series3: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clYellow
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
    object Series4: TLineSeries
      Marks.ArrowLength = 8
      Marks.Visible = False
      SeriesColor = clBlue
      Pointer.InflateMargins = True
      Pointer.Style = psRectangle
      Pointer.Visible = False
      XValues.DateTime = False
      XValues.Name = 'X'
      XValues.Multiplier = 1.000000000000000000
      XValues.Order = loAscending
      YValues.DateTime = False
      YValues.Name = 'Y'
      YValues.Multiplier = 1.000000000000000000
      YValues.Order = loNone
    end
  end
  object Button3: TButton
    Left = 656
    Top = 21
    Width = 81
    Height = 25
    Caption = 'Plot'
    TabOrder = 6
    OnClick = Button3Click
  end
  object Edit1: TEdit
    Left = 384
    Top = 311
    Width = 185
    Height = 21
    TabOrder = 7
    Text = '0,001'
  end
  object Edit2: TEdit
    Left = 656
    Top = -33
    Width = 81
    Height = 21
    TabOrder = 8
  end
  object Edit3: TEdit
    Left = 384
    Top = 287
    Width = 185
    Height = 21
    TabOrder = 9
  end
  object Button4: TButton
    Left = 568
    Top = 53
    Width = 81
    Height = 25
    Caption = 'Del'
    TabOrder = 10
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 408
    Top = 183
    Width = 97
    Height = 25
    Caption = 'Plot'
    TabOrder = 11
    OnClick = Button5Click
  end
  object Button7: TButton
    Left = 408
    Top = 239
    Width = 97
    Height = 25
    Caption = 'Plot'
    TabOrder = 12
    OnClick = Button7Click
  end
  object B1: TButton
    Left = 440
    Top = 119
    Width = 89
    Height = 25
    Caption = 'AddRow'
    TabOrder = 13
    OnClick = B1Click
  end
  object edt2: TEdit
    Left = 656
    Top = 114
    Width = 81
    Height = 21
    TabOrder = 14
  end
  object edt3: TEdit
    Left = 656
    Top = 90
    Width = 81
    Height = 21
    TabOrder = 15
  end
  object edt4: TEdit
    Left = 656
    Top = 136
    Width = 81
    Height = 21
    TabOrder = 16
  end
  object StringGrid4: TStringGrid
    Left = 16
    Top = 215
    Width = 177
    Height = 113
    ColCount = 4
    DefaultColWidth = 40
    FixedCols = 0
    RowCount = 4
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 17
  end
  object StringGrid2: TStringGrid
    Left = 200
    Top = 215
    Width = 177
    Height = 113
    ColCount = 4
    DefaultColWidth = 40
    FixedCols = 0
    RowCount = 4
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 18
  end
  object StringGrid3: TStringGrid
    Left = 16
    Top = 343
    Width = 177
    Height = 113
    ColCount = 4
    DefaultColWidth = 40
    FixedCols = 0
    RowCount = 4
    FixedRows = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 19
  end
  object btn1: TButton
    Left = 208
    Top = 352
    Width = 75
    Height = 25
    Caption = 'btn1'
    TabOrder = 20
    OnClick = btn1Click
  end
  object btn2: TButton
    Left = 208
    Top = 384
    Width = 75
    Height = 25
    Caption = 'btn2'
    TabOrder = 21
    OnClick = btn2Click
  end
end
